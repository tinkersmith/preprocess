<?php

namespace Drupal\preprocess\Utility;

/**
 * Interface for theme registry helpers.
 *
 * Theme registry helpers are utility classes that apply preprocess plugins to
 * to the theme registry.
 */
interface ThemeRegistryHelperInterface {

  /**
   * Alters the theme registry with preprocess plugins.
   *
   * @param array $theme_registry
   *   Reference to the theme registry to apply the preprocess plugins to.
   */
  public function alterRegistry(array &$theme_registry): void;

}
