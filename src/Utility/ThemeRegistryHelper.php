<?php

namespace Drupal\preprocess\Utility;

use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\preprocess\Preprocessor;
use Drupal\preprocess\PreprocessPluginManagerInterface;

/**
 * The default implementation of the theme registry helper.
 */
class ThemeRegistryHelper implements ThemeRegistryHelperInterface {

  /**
   * The preprocess plugin manager.
   *
   * @var \Drupal\preprocess\PreprocessPluginManagerInterface
   */
  protected $preprocessManager;

  /**
   * The theme manager.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $themeManager;

  /**
   * Create a new instance of the ThemeRegistryHelper class.
   *
   * @param \Drupal\preprocess\PreprocessPluginManagerInterface $preprocess_manager
   *   The preprocess plugin manager.
   * @param \Drupal\Core\Theme\ThemeManagerInterface $theme_manager
   *   The theme manager.
   */
  public function __construct(PreprocessPluginManagerInterface $preprocess_manager, ThemeManagerInterface $theme_manager) {
    $this->preprocessManager = $preprocess_manager;
    $this->themeManager = $theme_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function alterRegistry(array &$theme_registry): void {
    /** @var \Drupal\Core\Theme\ActiveTheme  $activeTheme */
    $activeTheme = $this->themeManager->getActiveTheme();
    $themeName = $activeTheme->getName();
    $themes = array_keys($activeTheme->getBaseThemeExtensions());
    $themes[] = $themeName;

    // Create a pattern that inserts the module provided preprocessors before
    // any theme hook, or before a preprocess hook that is more specific.
    $modulePattern = '/^(?:' . implode('|', $themes) . ')_preprocess_{{HOOK}}|_preprocess_{{HOOK}}__\w/';

    // Apply module defined preprocess plugins separately and before theme
    // any defined plugins to ensure that plugin defined hooks are added before
    // the theme hooks, so theme implementations can inherit these base hooks.
    // @see self::addMissingThemeHooks()
    $moduleDefs = $this->preprocessManager->getModuleDefinitions();
    $this->doAlter($theme_registry, $moduleDefs, $modulePattern);

    // Apply plugins defined by the currently active theme.
    $themeDefs = $this->preprocessManager->getDefinitionsByTheme($themeName);
    $this->doAlter($theme_registry, $themeDefs, '/_preprocess_{{HOOK}}__\w/');
  }

  /**
   * Sort plugin definitions by the theme hook they implement.
   *
   * Returns the preprocess plugin IDs sorted by the theme hook the preprocess
   * plugin applies to.
   *
   * @param \Drupal\preprocess\PreprocessDefinition[] $definitions
   *   The preprocess definitions to sort. Typically these are either
   *   definitions from modules OR the active theme.
   *
   * @return array[]
   *   Preprocess plugin IDs sorted by the theme hook them belong to.
   */
  protected function sortDefinitionsByHook(array $definitions): array {
    $sorted = [];
    foreach ($definitions as $id => $def) {
      foreach ($def->getHooks() as $hook) {
        $sorted[$hook][$id] = $id;
      }
    }

    return $sorted;
  }

  /**
   * Add preprocess plugin from $definitions to the theme registry.
   *
   * @param array $theme_registry
   *   Reference to the theme registry. This will be altered to include the
   *   calls to the preprocess plugins.
   * @param array $definitions
   *   Preprocess plugin definitions to insert into the $theme_registry.
   * @param string $pattern
   *   Regular expression to use when finding the place to insert theme
   *   preprocessors. Use the placeholder "{{HOOK}}" to be replaced with the
   *   current hook being placed. When a matching function name is found, the
   *   preprocessor will be placed before the matching preprocess function.
   */
  protected function doAlter(array &$theme_registry, array $definitions, $pattern) {
    // Get the module defined preprocess plugins, and sorted by their hooks.
    $byHook = $this->sortDefinitionsByHook($definitions);

    // Create a pattern that inserts the module provided preprocessors before
    // any theme hook, or before a preprocess hook that is more specific.
    foreach ($theme_registry as $hook => &$info) {
      $this->addHookPreprocessors($byHook, $hook, $info, $pattern);
    }

    // Add any hooks that are not already in the theme registry.
    // This happens when theme hook is a hook suggestion only defined by a
    // preprocess plugin. If a template or a hook_preprocess_HOOK() is defined
    // the theme hook will already be defined.
    foreach (array_diff_key($byHook, $theme_registry) as $hook => $pluginIds) {
      // Using ungreedy search, we peel back the last theme pattern separator.
      $prefix = preg_replace('/__[a-z_]+$/U', '', $hook, 1, $count);

      // We are looking for the longest prefix matching hook or closest
      // theme hook to base our new hook from. If we can't find a base theme
      // definition, the hook can't be added, but any valid theme definition
      // should at least have a theme function or template file and be defined.
      while ($count) {
        if (!empty($theme_registry[$prefix])) {
          $theme_registry[$hook] = $theme_registry[$prefix];
          $theme_registry[$hook]['base theme'] = preg_replace('/__.*$/', '', $prefix);
          $theme_registry[$hook]['preprocess functions'][] = new Preprocessor($hook, $pluginIds);
          break;
        }

        $prefix = preg_replace('/__[a-z_]+$/U', '', $prefix, 1, $count);
      }
    }
  }

  /**
   * Inserts the preprocessor based on the priority of the theme hook.
   *
   * This method adds the $preprocess into $preprocessors before the first
   * match of $insert_pattern. This method also advances the internal pointer
   * of $existing the preprocess callback array, while searching for the place
   * to add the preprocessor.
   *
   * @param \Drupal\preprocess\Preprocessor $preprocess
   *   The preprocessor instance to insert.
   * @param array $preprocessors
   *   Current list of preprocessors to insert the preprocessor into.
   * @param array $existing
   *   Reference to an array with the existing theme preprocessor callbacks.
   *   The internal pointer of this array is advanced to position of the
   *   inserted preprocessor.
   * @param string $insert_pattern
   *   Regular expression to use when finding the place to insert theme
   *   preprocessors. Use the placeholder "{{HOOK}}" to be replaced with the
   *   current hook being placed. When a matching function name is found, the
   *   preprocessor will be placed before the matching preprocess function.
   *
   * @see static::addHookPreprocessors()
   */
  private function insertPreprocessor(Preprocessor $preprocess, array &$preprocessors, array &$existing, $insert_pattern): void {
    $pattern = str_replace('{{HOOK}}', preg_quote($preprocess->getHook()), $insert_pattern);

    while ($callback = current($existing)) {
      // If matching the insert pattern, then place the preprocessor here,
      // but _DO NOT_ advance the existing funtion position. The next
      // preprocessor may still need to be placed before the next function.
      if (preg_match($pattern, $callback)) {
        $preprocessors[] = $preprocess;
        return;
      }

      $preprocessors[] = $callback;
      next($existing);
    }

    // Reached end of list, just add this preprocessor to the end of list.
    $preprocessors[] = $preprocess;
  }

  /**
   * Build a Preprocessor item for the theme hooks.
   *
   * @param string[][] $definitions
   *   Preprocess plugin definitions to attempt to apply to the theme registry.
   * @param string $hook
   *   The theme hook name.
   * @param array $info
   *   Theme definition information from the theme registry.
   * @param string $insert_pattern
   *   Regular expression to use when finding the place to insert theme
   *   preprocessors. Use the placeholder "{{HOOK}}" to be replaced with the
   *   current hook being placed. When a matching function name is found, the
   *   preprocessor will be placed before the matching preprocess function.
   */
  protected function addHookPreprocessors(array $definitions, $hook, array &$info, $insert_pattern) {
    $preprocessors = [];
    $prefixes = explode('__', $hook);
    $targetHook = reset($prefixes);
    $existing = $info['preprocess functions'] ?? [];
    reset($existing);

    // Apply any preprocess plugins defined for the base hook.
    // If not already the base hook, a base hook should be inserted before
    // any of the other hook implementations.
    $baseHook = $info['base hook'] ?? NULL;
    if ($baseHook && $baseHook !== $targetHook && !empty($definitions[$baseHook])) {
      $preprocess = new Preprocessor($baseHook, $definitions[$baseHook]);
      $this->insertPreprocessor($preprocess, $preprocessors, $existing, $insert_pattern);
    }

    do {
      if (!empty($definitions[$targetHook])) {
        $preprocess = new Preprocessor($targetHook, $definitions[$targetHook]);
        $this->insertPreprocessor($preprocess, $preprocessors, $existing, $insert_pattern);
      }

      $prefix = next($prefixes);
      $targetHook .= '__' . $prefix;
    } while ($prefix);

    // If preprocessors were added, put the remaining $existing callbacks
    // to the end of the preprocessors list, and set the new set of
    // $preprocessors as the "preprocess functions" of this theme hook.
    if (!empty($preprocessors)) {
      while ($callback = current($existing)) {
        $preprocessors[] = $callback;
        next($existing);
      }

      $info['preprocess functions'] = $preprocessors;
    }
  }

}
