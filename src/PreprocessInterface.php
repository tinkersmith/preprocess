<?php

namespace Drupal\preprocess;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * The interface for preprocess plugins.
 */
interface PreprocessInterface extends PluginInspectionInterface {

  /**
   * Applay a theme preprocess to renderable element.
   *
   * The preprocess hooks are also called with 2 additional parameters that can
   * be utilized by subclasses.
   *
   *  - string $hook: The theme hook calling this preprocessor.
   *  - array $theme_info: The theme_hook definition and info about the theme.
   *
   * These are available but left out of the base interface to keep backwards
   * compatibility with existing preprocess plugins.
   *
   * @param array $variables
   *   Reference to the theme $variables array.
   *
   * @return array
   *   The processed $variables array.
   */
  public function preprocess(array $variables): array;

}
