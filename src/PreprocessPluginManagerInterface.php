<?php

namespace Drupal\preprocess;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Interface for theme preprocess plugin managers.
 */
interface PreprocessPluginManagerInterface extends PluginManagerInterface {

  /**
   * Plugin priority comparison callback meant to be used with uasort().
   *
   * @return int
   *   Returns the comparison result of comparing plugin priority of $a to the
   *   plugin priority of $b. As standard for comparison callbacks to uasort()
   *   a negative value means priority of $a is less than that of $b. Zero means
   *   they are equal. A positive value means the priority of $a is larger than
   *   that of $b.
   *
   * @see uasort()
   */
  public static function comparePluginPriority(PreprocessDefinition $a, PreprocessDefinition $b): int;

  /**
   * Get plugin definitions that are provided by modules.
   *
   * @return array
   *   An array of plugin definitions that are provided by modules. Definitions
   *   are keyed by their plugin IDs.
   */
  public function getModuleDefinitions(): array;

  /**
   * Get plugin definitions provided by the requested theme.
   *
   * @param string $theme
   *   Machine name of theme to fetch the plugin definitions for.
   *
   * @return array
   *   An array of plugin definitions that were provided by the requested theme.
   *   Definitions are keyed by their plugin IDs. An empty array can be returned
   *   if there were no plugins provided by the requested theme.
   */
  public function getDefinitionsByTheme($theme): array;

}
