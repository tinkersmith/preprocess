<?php

namespace Drupal\preprocess;

use Drupal\Core\Plugin\PluginBase;

/**
 * Default base class for preprocess plugins.
 */
abstract class PreprocessPluginBase extends PluginBase implements PreprocessInterface {

}
