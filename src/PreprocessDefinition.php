<?php

namespace Drupal\preprocess;

use Drupal\Component\Plugin\Definition\DerivablePluginDefinitionInterface;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\themespace\Plugin\Definition\ProviderTypedPluginDefinition;

/**
 * Plugin definition class for theme preprocess plugins.
 */
class PreprocessDefinition extends ProviderTypedPluginDefinition implements DerivablePluginDefinitionInterface {

  /**
   * Deriver class for preprocess plugin definitions.
   *
   * Deriver class should implement
   * \Drupal\Component\Plugin\Derivative\DeriverInterface and return an array
   * of \Drupal\preprocess\PreprocessDefinition instances.
   *
   * @var string
   */
  protected $deriver;

  /**
   * The priority of this definition in relation to other preprocess plugins.
   *
   * This priority value helps to determine the order of preprocess plugins
   * which implement the same preprocess hook and of the same extension type
   * (module defined plugins always run before theme defined plugins).
   *
   * Plugins are sorted by ascending order of priority.
   *
   * @var int
   */
  protected $priority = 0;

  /**
   * The theme hooks that the preprocess plugin applies to.
   *
   * @var string[]
   */
  protected $hooks;

  /**
   * Create a new instance of the PreprocessDefinition class.
   *
   * @param array $definition
   *   The plugin definition values to set for this definition.
   *
   * @see \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function __construct(array $definition) {
    if (empty($definition['hook']) || empty($definition['provider_type'])) {
      $msg = sprintf('Preprocess "%s" definition is missing one of: hooks, provider type.', $definition['id']);
      throw new InvalidPluginDefinitionException($msg);
    }

    $this->id = $definition['id'];
    $this->class = $definition['class'];
    $this->provider = $definition['provider'];
    $this->providerType = $definition['provider_type'];

    // Apply priority if a value was provided, otherwise keep the default value.
    if (!empty($definition['priority'])) {
      $this->priority = $definition['priority'];
    }

    // Ensure that the hooks are always an array of hook values.
    if (!is_array($definition['hook'])) {
      $definition['hook'] = [$definition['hook']];
    }
    $this->hooks = $definition['hook'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDeriver() {
    return $this->deriver;
  }

  /**
   * {@inheritdoc}
   */
  public function setDeriver($deriver) {
    $this->deriver = $deriver;
    return $this;
  }

  /**
   * The set priorty of this plugin definition (ascending order).
   *
   * The priority only is only compared and sorted against preprocess plugins
   * with the same theme hook and extension type (module or theme).
   *
   * So theme provided plugins and hooks that are more specific ("node__page" vs
   * "node") will still always applied later, regardless of this value.
   *
   * @return int
   *   The priority of this plugin definition, to use for sorting.
   */
  public function getPriority(): int {
    return $this->priority;
  }

  /**
   * Get the list of render hooks this preprocess plugin applies to.
   *
   * @return string[]
   *   The list of theme render hooks this plugin applies to.
   */
  public function getHooks(): array {
    return $this->hooks;
  }

}
