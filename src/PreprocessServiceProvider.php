<?php

namespace Drupal\preprocess;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;
use Drupal\themespace\Plugin\ProviderTypedPluginManagerTrait;

/**
 * Preprocess module service provider for shimming new module dependencies.
 *
 * The dependency on Themespace module was added after the release of the
 * preprocess module. This means that it is possible that the preprocess module
 * is already installed, without the themespace module being active.
 *
 * The hook_update_N (update hook "9201") installs this dependency, however, the
 * new PreprocessPluginManager crashes with a PHP error, which prevents Drupal
 * from running the updates. This service provider, substitutes the plugin
 * manager class with a placeholder, that allows the update hook to run properly
 * when the required Themespace classes and traits are not available.
 *
 * @see Drupal\preprocess\PreprocessPluginManager
 * @see Drupal\themespace\Plugin\ProviderTypedPluginManagerTrait
 */
class PreprocessServiceProvider implements ServiceModifierInterface {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    if (!trait_exists(ProviderTypedPluginManagerTrait::class)) {
      $definition = $container->getDefinition('plugin.manager.preprocess');
      $definition->setClass('Drupal\preprocess\DummyPluginManager');
    }
  }

}
