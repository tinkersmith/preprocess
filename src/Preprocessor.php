<?php

namespace Drupal\preprocess;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;

/**
 * Invokable class which applies a series of preprocess methods.
 */
class Preprocessor {

  /**
   * The preprocess plugin manager.
   *
   * @var \Drupal\preprocess\PreprocessPluginManagerInterface
   */
  protected static $preprocessManager;

  /**
   * The theme hook this preprocessor is designed to work with.
   *
   * @var string
   */
  protected $hook;

  /**
   * The list of preprocess plugin IDs which are invoked by this preprocessor.
   *
   * @var string[]
   */
  protected $pluginIds;

  /**
   * Get the preprocess plugin manager.
   *
   * @return \Drupal\preprocess\PreprocessPluginManagerInterface
   *   The preprocess plugin manager.
   *
   * @throws \Symfony\Component\DependencyInjection\Exception\RuntimeException
   * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
   */
  protected static function getPreprocessManager(): PreprocessPluginManagerInterface {
    if (!isset(static::$preprocessManager)) {
      static::$preprocessManager = \Drupal::service('plugin.manager.preprocess');
    }

    return static::$preprocessManager;
  }

  /**
   * Creates a new instance of the Preprocessor invokable object.
   *
   * @param string $hook
   *   The theme hook this preprocessor is meant to work on.
   * @param array $preprocess_ids
   *   The list of IDs to execute preprocess plugins for when invoked.
   */
  public function __construct($hook, array $preprocess_ids) {
    $this->hook = $hook;
    $this->pluginIds = $preprocess_ids;
  }

  /**
   * The string value is the theme hook name if this was a preprocess function.
   *
   * Creating a string name following the "hook_preprocess_HOOK" callback
   * convention, with "preprocess" for the module, allows it to have the same
   * precedence as a theme preprocess hook defined from the preprocess module.
   *
   * @return string
   *   The hook theme preprocess function name if this was a theme hook.
   */
  public function __toString(): string {
    return 'preprocess_preprocess_' . $this->hook;
  }

  /**
   * Get the theme hook name this preprocessor targets.
   *
   * @return string
   *   The hook this preprocessor is set to preprocess.
   */
  public function getHook() {
    return $this->hook;
  }

  /**
   * Method for when the preprocessor is invoked.
   *
   * Method is called during the preprocessing stage of theme element rendering
   * and applies the preprocess() method of the assigned preprocess plugins in
   * the order the same order they are listed in the $pluginIds property.
   *
   * @param array $variables
   *   Reference to the theme element variables to preprocess.
   * @param string $hook
   *   The string name of the theme preprocess hook that is being worked on.
   * @param array $theme_info
   *   The theme definition for the theme element being processed. Information
   *   include the template files, preprocess and process callbacks.
   */
  public function __invoke(array &$variables, $hook, array $theme_info = []) {
    $preprocessManager = static::getPreprocessManager();

    foreach ($this->pluginIds as $pluginId) {
      try {
        $plugin = $preprocessManager->createInstance($pluginId);
        $variables = $plugin->preprocess($variables, $hook, $theme_info);
      }
      catch (PluginNotFoundException $e) {
        // Plugin is missing because provider is probably uninstalled, but
        // the callback was still registered. It should be safe to just skip
        // this processor.
        //
        // Unlikely to happen, since cache rebuilds are part of module
        // or theme uninstalls and this generally should not be out of sync.
      }
    }
  }

}
