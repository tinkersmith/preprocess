<?php

namespace Drupal\preprocess\Annotation;

use Drupal\Component\Annotation\AnnotationInterface;
use Drupal\preprocess\PreprocessDefinition;
use Drupal\themespace\Annotation\ProviderTypedPlugin;

/**
 * Annotation for preprocess plugins.
 *
 * Preprocess plugins help to process theme elements before they are rendered
 * and can be triggered for modules or themes. Theme based plugins are only
 * applied when the theme that provides them is currently the active theme.
 *
 * Plugin Namespace: Plugin\Preprocess.
 *
 * @see \Drupal\Component\Annotation\Plugin
 * @see \Drupal\themespace\Annotation\ProviderTypedPluginInterface
 * @see \Drupal\preprocess\PreprocessInterface
 * @see \Drupal\preprocess\PreprocessPluginManagerInterface
 * @see plugin_api
 *
 * @ingroup preprocess_plugins
 *
 * @Annotation
 */
class Preprocess extends ProviderTypedPlugin implements AnnotationInterface {

  /**
   * The plugin ID of this preprocess plugin.
   *
   * @var string
   */
  public $id;

  /**
   * The theme hooks to apply this preprocess to.
   *
   * The definition supports a single hook as a string or an array of hooks
   * that this plugin may apply to. This is helpful in the cases where,
   * this preprocess applies to multiple theme suggestions.
   *
   * @var string|string[]
   */
  public $hook;

  /**
   * Value to use with sorting the order of preprocess plugins.
   *
   * This priority value helps to determine the order of preprocess plugins
   * which implement the same preprocess hook and of the same extension type
   * (module defined plugins always run before theme defined plugins).
   *
   * @var int
   */
  public $priority = 0;

  /**
   * {@inheritdoc}
   */
  public function get() {
    return new PreprocessDefinition($this->definition);
  }

}
