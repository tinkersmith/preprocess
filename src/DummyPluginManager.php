<?php

namespace Drupal\preprocess;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Placeholder plugin manager which allows database update hook to run.
 *
 * Mirrors the new preprocess plugin manager constructor, but purposely
 * doesn't cache or discovery any plugins. This is used if the themespace
 * module dependency isn't installed and is used as a placeholder plugin
 * manager to avoid crashing enough to allow the themespace module to be
 * activated.
 *
 * @see \Drupal\preprocess\PreprocessServiceProvider::alter()
 */
class DummyPluginManager extends DefaultPluginManager implements PreprocessPluginManagerInterface {

  /**
   * Create a new instance of the PreprocessPluginManager class.
   *
   * Needs to match the exact arguments of PreprocessPluginManager so the
   * arguments from preprocess.services.yml do not need to be altered.
   *
   * @param \Traversable $namespaces
   *   The container namespaces to search for preprocess plugins.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend for plugin definition storage.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Preprocess',
      $namespaces,
      $module_handler,
      'Drupal\preprocess\PreprocessInterface',
      'Drupal\preprocess\Annotation\Preprocess'
    );

    $this->setCacheBackend($cache_backend, 'preprocess');
  }

  /**
   * {@inheritdoc}
   */
  public static function comparePluginPriority(PreprocessDefinition $a, PreprocessDefinition $b): int {
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function findDefinitions() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitions() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getModuleDefinitions(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitionsByTheme($theme): array {
    return [];
  }

}
