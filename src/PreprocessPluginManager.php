<?php

namespace Drupal\preprocess;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\themespace\Plugin\Discovery\ProviderTypedAnnotatedClassDiscovery;
use Drupal\themespace\Plugin\Discovery\ProviderTypedDeriverDiscoveryDecorator;
use Drupal\themespace\Plugin\Discovery\ProviderTypedYamlDiscoveryDecorator;
use Drupal\themespace\Plugin\ProviderTypedPluginManagerTrait;

/**
 * Plugin manager for theme elements preprocess plugins.
 */
class PreprocessPluginManager extends DefaultPluginManager implements PreprocessPluginManagerInterface {

  use ProviderTypedPluginManagerTrait {
    findDefinitions as findTypedDefinitions;
  }

  /**
   * Create a new instance of the PreprocessPluginManager class.
   *
   * @param \Traversable $namespaces
   *   The container namespaces to search for preprocess plugins.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend for plugin definition storage.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler
   *   The theme handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, ThemeHandlerInterface $theme_handler) {
    parent::__construct(
      'Plugin/Preprocess',
      $namespaces,
      $module_handler,
      'Drupal\preprocess\PreprocessInterface',
      'Drupal\preprocess\Annotation\Preprocess'
    );

    $this->themeHandler = $theme_handler;
    $this->alterInfo('preprocess_info');
    $this->setCacheBackend($cache_backend, 'preprocess');
  }

  /**
   * {@inheritdoc}
   */
  public static function comparePluginPriority(PreprocessDefinition $a, PreprocessDefinition $b): int {
    return $a->getPriority() - $b->getPriority();
  }

  /**
   * {@inheritdoc}
   */
  public function clearCachedDefinitions() {
    parent::clearCachedDefinitions();

    // If definition caches have been cleared, also remove our currently
    // grouped plugin definitions.
    unset($this->moduleDefinitions);
    unset($this->themeDefinitions);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDiscovery() {
    if (!$this->discovery) {
      $discovery = new ProviderTypedAnnotatedClassDiscovery(
        $this->subdir,
        $this->namespaces,
        $this->pluginDefinitionAnnotationName,
        $this->additionalAnnotationNamespaces
      );
      $discovery = new ProviderTypedYamlDiscoveryDecorator(
        $discovery,
        'preprocessors',
        $this->moduleHandler->getModuleDirectories(),
        $this->themeHandler->getThemeDirectories(),
        PreprocessDefinition::class
      );
      $this->discovery = new ProviderTypedDeriverDiscoveryDecorator($discovery);
    }
    return $this->discovery;
  }

  /**
   * {@inheritdoc}
   */
  protected function findDefinitions() {
    $definitions = $this->findTypedDefinitions();

    // Sort the plugins by plugin definition priority value.
    uasort($definitions, static::class . '::comparePluginPriority');
    return $definitions;
  }

}
